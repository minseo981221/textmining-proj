import GetOldTweets3 as got

from bs4 import BeautifulSoup

import datetime

 

days_range = []

 

start = datetime.datetime.strptime("2019-09-01", "%Y-%m-%d")

end = datetime.datetime.strptime("2019-09-30", "%Y-%m-%d")


date_generated = [start + datetime.timedelta(days=x) for x in range(0,int((end-start).days)+1)]

 

for date in date_generated:

    days_range.append(date.strftime("%Y-%m-%d"))

    

print("=== 설정된 트윗 수집 기간은 {} 에서 {} 까지 입니다 ===".format(days_range[0], days_range[-1]))

import time

 

# 수집 기간 맞추기

start_date = days_range[0]

end_date = (datetime.datetime.strptime(days_range[-1], "%Y-%m-%d") 

            + datetime.timedelta(days=1)).strftime("%Y-%m-%d") # setUntil이 끝을 포함하지 않으므로, day + 1

 

# 트윗 수집 기준 정의

tweetCriteria = got.manager.TweetCriteria().setQuerySearch('남북통일')\
.setSince(start_date)\
.setUntil(end_date)\
.setMaxTweets(-1)

 

start_time = time.time()

tweet = got.manager.TweetManager.getTweets(tweetCriteria)

from random import uniform

from tqdm import tqdm_notebook

 

tweet_list = []

for index in tqdm_notebook(tweet):

    

    # 메타데이터 목록 

    username = index.username

    link = index.permalink 

    content = index.text

    tweet_date = index.date.strftime("%Y-%m-%d")

    tweet_time = index.date.strftime("%H:%M:%S")

    retweets = index.retweets

    favorites = index.favorites

    

    info_list = [tweet_date, tweet_time, username, content, link, retweets, favorites]

    tweet_list.append(info_list)

import pandas as pd
 
twitter_df = pd.DataFrame(tweet_list, 

                          columns = ["date", "time", "user_name", "text", "link", "retweet_counts", "favorite_counts"])

 

twitter_df.to_csv("sample_twitter_data_{}_to_{}.csv".format(days_range[0], days_range[-1]), index=False)

 

 

df_tweet = pd.read_csv('sample_twitter_data_{}_to_{}.csv'.format(days_range[0], days_range[-1]))

df_tweet.head(3)
