# 2019 TEXT-MINING TERM PROJECT

@Soonchunhyang University. Bigdata Engineering


1. 프로젝트 설명
-----------------------------
    1)정의
    - 남북통일에 대한 우리 국민들의 인식 파악하기
    - 북한 관련 주요 키워드 파악하기
    2)필요성
    - 통일에 대한 인식을 조사한 자료가 타당한지 데이터로 증명하기

2. 트위터 감정 분석
---------------------------------
    목표: 통일에 대한 감정(인식)을 분류하기
    1)데이터 수집
        직접 감정 값을 입력함(0:부정 1:긍정 2:중립)
    2)머신러닝을 통해 가장 높은 정확도가 나오는 모델 선정
        -> 랜덤포레스트
|          |  MLP  | Logistic Regression | Random Forest |  SVM  |
|----------| ----- | ------------------- | ------------- | ----- |
| accuracy | 51.7% |        52.5%        |     63.2%     | 34.2% |
    3)한계점
    정확한 감정 값을 판단하기 어려움
            
            
3. 핵심 키워드 분석
---------------------------
    한반도 통일 동향을 살펴보기
    1) 데이터 수집
        네이버 뉴스(2016~2019년)
        청와대 국민청원
    2) 데이터 분석과정
        -트위터 형태소 분석기 사용
        -명사와 형용사만 선택해 리스트 구성
        -각 형태소별 빈도수 파악
        -워드클라우드 생성
    3) 정부별 핵심 키워드 확인하기
        -워드클라우드
    4) 국민청원의 키워드 흐름 파악
        -그래프
        
4. 향후 계획
--------
감정 분류기의 성능을 향상시켜 국민들이 SNS나 댓글에 작성한 글을 통해
특정 이슈를 중심으로 국민들의 인식(긍정/부정)흐름 파악하기
